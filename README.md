# Node Ditto

Proof of concept, control Ditto (Vibrating Notification Device by Simple Matters) using node

## Patterns

| name     | hex    | binary           | notes |
| -------- | ------ | ---------------- | ----- |
| one      | 0x1000 | 0001000000000000 |       |
| two      | 0x1100 | 0001000100000000 |       |
| three    | 0x1110 | 0001000100010000 |       |
| four     | 0x1111 | 0001000100010001 |       |
| fallback | 0x4470 | 0100010001110000 |       |
| five     | 0x5570 | 0101010101110000 |       |
| '3-2'    | 0x545c | 0101010001011100 |       |
| '2-1-2'  | 0x5117 | 0101000100010111 |       |
