const noble = require('@abandonware/noble');

const DITTO_SERVICE = '4AB97A4C63F04C7A85F0AFB07CE65D41';
const TEST_VIBRATION_CHARACTERISTIC = '4AB97A4F63F04C7A85F0AFB07CE65D41';

const PATTERNS = {
  one: 0x1000,
  two: 0x1100,
  three: 0x1110,
  four: 0x1111,
  fallback: 0x4470,
  five: 0x5570,
  '3-2': 0x545c,
  '2-1-2': 0x5117,
};

noble.on('stateChange', async state => {
  if (state === 'poweredOn') {
    await noble.startScanningAsync([DITTO_SERVICE], false);
  }
});

noble.on('discover', onDiscover);

async function onDiscover(peripheral) {
  const {address, advertisement} = peripheral;
  try {
    await noble.stopScanningAsync();
    await peripheral.connectAsync();
    console.log(`Found device: ${address}`);
    const [dittoService] = await peripheral.discoverServicesAsync([
      DITTO_SERVICE,
    ]);
    if (!dittoService) {
      console.log("didn't find ditto service");
      return;
    }
    const [
      vibrationCharacteristic,
    ] = await dittoService.discoverCharacteristicsAsync([
      TEST_VIBRATION_CHARACTERISTIC,
    ]);
    if (!vibrationCharacteristic) {
      console.log("didn't find vibration characteristic");
      return;
    }

    await sendVibration(vibrationCharacteristic);
    await peripheral.disconnectAsync();
  } catch (err) {
    console.log('Exception', err);
  }

  process.exit(0);
}

async function sendVibration(vibrationCharacteristic) {
  const pattern = PATTERNS[process.argv[2]] || PATTERNS.fallback;
  const repeat = 1;

  const command = Buffer.alloc(3);
  command.writeUInt16LE(pattern, 0);
  command[2] = repeat;

  await vibrationCharacteristic.writeAsync(command, false);
}
